namespace CodeFirstDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FavoriteColor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "FavoriteColor", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "FavoriteColor");
        }
    }
}
