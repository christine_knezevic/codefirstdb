//namespace CodeFirstDb.Migrations
//{
//    using System;
//    using System.Data.Entity.Migrations;
    
//    public partial class StudentAdress : DbMigration
//    {
//        public override void Up()
//        {
//            CreateTable(
//                "dbo.StudentAddresses",
//                c => new
//                    {
//                        StudentAddressId = c.Int(nullable: false, identity: true),
//                        StudentId = c.Int(nullable: false),
//                        AddressId = c.Int(nullable: false),
//                        InactiveDate = c.DateTime(),
//                    })
//                .PrimaryKey(t => t.StudentAddressId)
//                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
//                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
//                .Index(t => t.StudentId)
//                .Index(t => t.AddressId);
            
//            CreateTable(
//                "dbo.Students",
//                c => new
//                    {
//                        StudentId = c.Int(nullable: false, identity: true),
//                        DateOfBirth = c.DateTime(),
//                        FirstName = c.String(nullable: false),
//                        LastName = c.String(nullable: false),
//                    })
//                .PrimaryKey(t => t.StudentId);
            
//        }
        
//        public override void Down()
//        {
//            DropForeignKey("dbo.StudentAddresses", "StudentId", "dbo.Students");
//            DropForeignKey("dbo.StudentAddresses", "AddressId", "dbo.Addresses");
//            DropIndex("dbo.StudentAddresses", new[] { "AddressId" });
//            DropIndex("dbo.StudentAddresses", new[] { "StudentId" });
//            DropTable("dbo.Students");
//            DropTable("dbo.StudentAddresses");
//        }
//    }
//}
