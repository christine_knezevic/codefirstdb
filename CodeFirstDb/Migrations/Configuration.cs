namespace CodeFirstDb.Migrations
{
    using CodeFirstDb.Data;
    using CodeFirstDb.Models;
    using System.Data.Entity.Migrations;  
    using System;
    using System.Linq;
  using System.Collections.Generic; 

    internal sealed class Configuration : DbMigrationsConfiguration<CodeFirstDb.Data.CodeFirstDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CodeFirstDbContext context)
        {
          SeedPerson(context, "Cecelia", "Hedrick", new DateTime(1986, 1, 24), "Slytherin", "123 Any St", (string)null, 
            "Papillion", "NE", "68846", "Black");
          SeedPerson(context, "Dillon", "Gitano", new DateTime(1987, 7, 2), "Hufflepuff", "123 Hogwarts Ln", "Apt 9", 
            "Omaha", "NE", "68105", "Turquoise");

          var johnnyIsInTheDb = context.Students.Any(x => x.FirstName.Equals("John") && x.LastName.Equals("Temple"));

          if (!johnnyIsInTheDb)
          {
            //create new student
            var student = new Student();

            student.DateOfBirth = new DateTime(2006, 12, 23, 14, 22, 0);
            student.FirstName = "John";
            student.LastName = "Temple";

            //create new studentaddress
            var studentAddress = new StudentAddress();

            //set studentaddressproperties
            studentAddress.InactiveDate = null;

            //create new address
            var address = new Address();

            //set address properties
            address.Address1 = "999 Ninety Nine ST";
            address.City = "Papillion";
            address.State = "NE";
            address.ZipCode = "68046-8047";

            //hook address to studentaddress
            studentAddress.Address = address;

            //hook studentaddress to student
            student.StudentAddresses = new List<StudentAddress>();

            student.StudentAddresses.Add(studentAddress);

            //let's say Johnny has two addresses, but this one is inactive
            var studentAddressOld = new StudentAddress();

            studentAddressOld.InactiveDate = DateTime.Now;

            var addressOld = new Address();

            addressOld.Address1 = "111 Elventy Eleven St";
            addressOld.City = "Council Bluffs";
            addressOld.State = "IA";
            addressOld.ZipCode = "51501";

            studentAddressOld.Address = addressOld;

            student.StudentAddresses.Add(studentAddressOld);

            context.Students.Add(student);
          }

         
        }

        private static void SeedPerson(CodeFirstDbContext context, string firstName, string lastName, DateTime dateOfBirth, 
          string title, string address1, string address2, string city, string state, string zipCode, string favoriteColor)
        {
          //fetch the person out of the db with the given first/last name
          var person = context.People.Where(x => x.FirstName == firstName && x.LastName == lastName).SingleOrDefault();

          //did we get a person?
          if(person != null)
          {
            
              person.FirstName = firstName;
              person.LastName = lastName;
              person.DateOfBirth = dateOfBirth;
              person.Title = title;
              person.FavoriteColor = favoriteColor;
              
              person.Address.Address1 = address1;
              person.Address.Address2 = address2;
              person.Address.City = city;
              person.Address.State = state;
              person.Address.ZipCode = zipCode;
          }
          else
            {
              //add a new person
              person = new Person
              {
                FirstName = firstName,
                LastName = lastName,
                DateOfBirth = dateOfBirth,
                Title = title,
                FavoriteColor = favoriteColor
              };
            var address = new Address
              {
                Address1 = address1,
                Address2 = address2,
                City = city,
                State = state,
                ZipCode = zipCode
              };
              person.Address = address;
              context.People.Add(person);
            }
          
          }
     }
}
