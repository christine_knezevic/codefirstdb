﻿using CodeFirstDb.Models;
using System.Data.Entity;

namespace CodeFirstDb.Data
{
  class CodeFirstDbContext : DbContext
  {
    public CodeFirstDbContext()
      : base("name = CodeFirstDbConnection")
    { 
      
    }

    public DbSet<Person> People { get; set; }
    public DbSet<Address> Addresses { get; set; }
    public DbSet<Student> Students { get; set; }
    public DbSet<StudentAddress> StudentAddresses { get; set; }
    }
}
