﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDb.Models
{
  public class StudentAddress
  {
    public int StudentAddressId { get; set; }

    public int StudentId { get; set; }

    public int AddressId { get; set; }

    public DateTime? InactiveDate { get; set; }

    public virtual Student Student { get; set; }

    public virtual Address Address { get; set; }
  }
}
