﻿using System;
using System.ComponentModel.DataAnnotations;


namespace CodeFirstDb.Models
{
  public class Address
  {

    public int AddressId { get; set; }
    
    [Required]
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    
    [Required]
    public string City { get; set; }

    [Required]
    [MaxLength(2)]
    public string State { get; set; }
    
    [Required]
    public string ZipCode { get; set; }
  }
}
