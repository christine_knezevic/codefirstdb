﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstDb.Models
{
  public class Person
  {
    public int PersonId{get; set;}
    
    public int AddressId { get; set; }

    public DateTime DateOfBirth { get; set; }

    public string FavoriteColor { get; set; }

    public string Title { get; set; }
    
    [Required]
    public string FirstName { get; set; }
    
    [Required]
    public string LastName { get; set; }
    
    public virtual Address Address { get; set; }
  }
}
